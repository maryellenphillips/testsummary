# TestSummary
Summarizes A110 test data from an .MT file into a formatted Excel spreadsheet with the push of a button.

##### Required Python Packages:
[MultiTimeProcessing](https://gitlab.com/agile-space/data-processing/-/tree/master/MultiTimeProcessing) == 0.0.1\
XLWings == 0.25.0\
NumPy == 1.21.4

These instructions are repeated in the 'General Info & How-to' sheet of the testsummary.xlsm sheet for ease of use.
## Create a Virtual Environment & Install Necessary Packages
**You will need access to the Agile GitLab data-processing repository**\
In your command prompt, navigate to the folder that you want to store your virtual environment and create the environment

*python -m venv venv*

Activate your environment

*venv\Scripts\activate.bat*

You will know if the virtual environment is active if the command line begins with (venv)\
With your virtual environment active, navigate to the TestSummary repository base folder

*pip install -r requirements.txt*

Lastly follow the instructions on the README.md file to install MultiTimeProcessing [here](https://gitlab.com/agile-space/data-processing/-/tree/master/MultiTimeProcessing).
## How to Install XLWings Add-In for Excel
With your virtual environment created active

*xlwings addin install*

Open the testsummary.xlsm. If you are asked to, enable macros.\
Open the VBA editor which is found in the 'Developer' tab. If you do not have a 'Developer' tab, open VBA using the keyboard shortcut Alt + F11\
Then, in the VBA editor, go to Tools>References and make sure the box next to xlwings is checked.
## How to Use This Workbook
*These instructions need to be updated*

1. Navigate to the '_xlwings.conf' sheet and follow all instructions on the sheet.
2. In the 'Trim' sheet, enter the filepath for the directory that will hold all data files in the cell labelled "ROOT DIRECTORY" **Use TWO slashes!**
3. Remaining on the 'Trim' sheet, unhide row 9 and check the PIDs for each column. Change any PIDs as necessary.
4. Remaining on the 'Trim' sheet, enter the start and stop time over which you want summarized data as data is collected.
5. Ensure that the cells below the start and stop time you have just entered are empty. This is how the script will know which line to fill with data.
6. If you are importing data for multiple pulses, you may differentiate the pulses with certain IDs as long as there is a space at the end of the test id number
7. Click the "IMPORT DATA" button. The TestID and status of the data import is shown in the top left corner in cell A1. If the .MT file is not found, check your root directory and/or file names for test data.
## Changing PIDs
If a PID is not found for a particular test, the appropriate cell will be left blank. In the 'Trim' and 'IspCalc' sheets, the PIDs are stored in a hidden row directly below the column headers.
To change the PID that the script will pull data for, unhide this row and replace the information as needed.

## Common Errors
*still writing*
