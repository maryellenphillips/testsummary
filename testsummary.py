# Author: Mary Ellen J. Phillips
# Email: mary-ellen.phillips@agilespaceindustries.com

import xlwings as xw
import numpy as np
import MultiTimeProcessing as mtp
import os
import re

def main():
    # Define workbook and sheets
    wb = xw.Book.caller()       
    trim = wb.sheets.active
    ispCalc = wb.sheets['IspCalc']
    
    # Find last row
    lastrow = trim.range('E9').end('down').row 
    lastrow_isp = ispCalc.range('A2').end('down').row

    # Get PIDs, TestID, from spreadsheet:
    testid_cell = 'D' + str(lastrow)
    testid = (trim.range(testid_cell).value.split())[0]
    PIDs = trim.range('G9:S9').value

    # Find .MT file for testid
    rootDir = trim.range('C4').value + '\\' + testid
    regex = re.compile(r".*\.MT$")
    for root, directories, files in os.walk(rootDir):
        for file in files:
            if regex.match(file):
                filepath = os.path.join(rootDir,file)

    # Get .MT data & fill cells
    try: 
        i = 0
        for i in range(lastrow):
            checkcell = 'D' + str(lastrow - i)
            
            # Display message 
            trim.range('A1').value = trim.range(checkcell).value + ': Gathering data...'
            ispCalc.range('A1').value = trim.range(checkcell).value + ': Gathering data...'

            if trim.range(checkcell).value.split()[0] == testid: 
                # Define fill row, start & stop times
                fillrow = trim.range(checkcell).row 
                fill_cell = 'G' + str(fillrow)
                start_cell = 'E' + str(fillrow)
                stop_cell = 'F' + str(fillrow)
                start = trim.range(start_cell).value
                stop = trim.range(stop_cell).value
                
                # Fill 'Trim'
                fillList = []
                for PID in PIDs:
                    if PID == "InjectordPOX":
                        fillList.append('=(I{0}-R{0})*100/R{0}'.format(fillrow))
                    elif PID == "InjectordPFU":
                        fillList.append('=(O{0}-R{0})*100/R{0}'.format(fillrow))
                    else:
                        try:
                            fillList.append(np.average(mtp.get_MT_data(filepath,PID,start,stop)))
                        except ValueError:
                            fillList.append(None)
                trim.range(fill_cell).value = fillList

                # Fill 'IspCalc'
                for j in range(1, lastrow_isp + 1):
                    if trim.range(checkcell).value in str(ispCalc.range('A{}'.format(j)).value):
                        try:
                            thrust = np.average(mtp.get_MT_data(filepath,ispCalc.range('E3').value,start,stop))
                        except ValueError:
                            thrust = None
                        try:
                            cellPress = np.average(mtp.get_MT_data(filepath,ispCalc.range('I3').value,start,stop))
                        except ValueError:
                            cellPress = None
                        ispCalc.range('E{}'.format(j)).value = thrust 
                        ispCalc.range('I{}'.format(j)).value = cellPress 
                
            else:
                # Display message
                trim.range('A1').value = testid + ': Import complete'
                ispCalc.range('A1').value = testid + ': Import complete'
                break
    except:
            trim.range('A1').value = testid + ': .MT file not found' 
            ispCalc.range('A1').value = testid + ': .MT file not found'

if __name__ == "__main__":
    xw.Book("testsummary.xlsm").set_mock_caller()
    main()
